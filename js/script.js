let EVENTS_LIST = null;

function sendServerRequest() {
    const serverURL = 'http://127.0.0.1:5000/events';

    let reqObj = {};
    reqObj.whitelist = $('#search').val();
    reqObj.blacklist = $('#blacklist').val();

    let serverReqeustData = JSON.stringify(reqObj);

    // START server request ====
    $.post(serverURL, reqObj, function(res) {
        EVENTS_LIST = res;
        serverCallbacks(res);
    });
    // ====

    // SIMULATE CALLBACK ====
    /* 
    const response = exampleServerResponse; // !!!REMOVE THIS!!!
    EVENTS_LIST = response;
    serverCallbacks(response); 
    */
    // ====
}

function serverCallbacks(response) {
    $('#search').val(''); // reset the field
    $('#blacklist').val(''); // reset the field

    $('.media:not(#event-item-template)').remove(); // reset list body

    // response = array[], cycle through each item to create a new event
    response.forEach((eventItem) => {
        buildList(eventItem);
    });
}

function buildList(response='') {
    // const template = $('#event-item-template');

    // Setup the template =====
    $('#event-item-template .img-container').css('background-image','url('+response.image_url+')'); // update thumbnail image
    $('#event-item-template h4').html(response.name);
    $('#event-item-template .media-event-organizer').html(response.organizer);
    $('#event-item-template .media-event-location').html(response.venue);
    $('#event-item-template .media-event-description').html(response.description);
    $('#event-item-template .media-event-date').html(response.date);
    // $('#event-item-template media-event-timestamp').html(response.created_timestamp);
    $('#event-item-template .media-event-url').html(response.source);
    $('#event-item-template .media-event-url').attr('href',response.url);
    // =====

    // Clone the template ====
    let new_list_item = $('#event-item-template').clone();
    new_list_item.removeClass('d-none');
    new_list_item.attr('id', response.id);
    new_list_item.attr('data-event-id', response.id);
    // ====

    // Append new_list_item ====
    $('#events-list-react').append(new_list_item);
    // ====
}

function showDetailModal(callerElement) {
    let eventId = $(callerElement).data('event-id');
    let selected_event = EVENTS_LIST.filter((event) => {
        return event.id == eventId;
    })[0];

    $('#modal-detail-view .modal-title').html(selected_event.name);
    $('#modal-detail-view .detail-modal-date').html(selected_event.date);
    $('#modal-detail-view .detail-modal-location').html(selected_event.venue);

    $('#modal-detail-view .detail-modal-description').html(selected_event.description);

    $('#modal-detail-view .detail-modal-url a').html(selected_event.source);
    $('#modal-detail-view .detail-modal-url a').attr('href',selected_event.url);
    $('#modal-detail-view .detail-modal-organizer').html(selected_event.organizer);

    if(selected_event.num_rsvp) { // only if num_rvsp is true (meaning it's an integer), then we want to execute this
        $('#modal-detail-view .detail-modal-rsvp').html(selected_event.num_rsvp);
        $('#modal-detail-view .detail-modal-rsvp').removeClass('d-none');
    }

    $('#modal-detail-view').modal();
}

function highlight(text, highlightedTerm) {
    let outputText = '';
    let currentIndex = 0;
    let index = inputText.indexOf(highlightedTerm);
    if (index >= 0) { 
     innerHTML = innerHTML.substring(0,index) + "<span class='highlight'>" + innerHTML.substring(index,index+text.length) + "</span>" + innerHTML.substring(index + text.length);
     inputText.innerHTML = innerHTML;
    }
  }