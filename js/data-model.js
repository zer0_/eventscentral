const exampleEvent1 = {
    id: 1,
    name: "New Event",
    description: "Warang mayang sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eu lobortis elementum nibh tellus molestie nunc non. In pellentesque massa placerat duis ultricies lacus sed. Morbi enim nunc faucibus a pellentesque. Id consectetur purus ut faucibus pulvinar elementum. Nisi est sit amet facilisis magna etiam tempor orci. Leo urna molestie at elementum eu. Nullam ac tortor vitae purus faucibus ornare suspendisse sed. Quam id leo in vitae. Turpis in eu mi bibendum neque egestas congue quisque egestas. Eget velit aliquet sagittis id consectetur purus ut faucibus. Donec ac odio tempor orci dapibus ultrices in iaculis nunc. In arcu cursus euismod quis viverra nibh cras. Velit ut tortor pretium viverra suspendisse potenti. Quam adipiscing vitae proin sagittis nisl rhoncus mattis. Diam quam nulla porttitor massa id neque aliquam. Urna condimentum mattis pellentesque id nibh tortor id aliquet lectus. Diam ut venenatis tellus in metus. Commodo quis imperdiet massa tincidunt nunc pulvinar sapien. Id venenatis a condimentum vitae sapien. Quam pellentesque nec nam aliquam sem et. Vitae tempus quam pellentesque nec nam aliquam sem et. Porttitor leo a diam sollicitudin. Amet mauris commodo quis imperdiet massa tincidunt. Eu nisl nunc mi ipsum faucibus vitae. Lacus sed turpis tincidunt id aliquet risus feugiat in ante. Ornare aenean euismod elementum nisi quis eleifend quam. Facilisis mauris sit amet massa. Urna nunc id cursus metus aliquam eleifend mi in nulla. Sit amet venenatis urna cursus eget nunc scelerisque viverra. Sed risus ultricies tristique nulla aliquet enim tortor at auctor. Consequat semper viverra nam libero justo laoreet. Sit amet dictum sit amet. Consequat semper viverra nam libero justo laoreet sit. Nisi est sit amet facilisis. Amet nisl purus in mollis nunc sed. Dignissim diam quis enim lobortis scelerisque. Fermentum et sollicitudin ac orci phasellus egestas tellus rutrum tellus. Amet porttitor eget dolor morbi non arcu risus. Vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt. Ultricies tristique nulla aliquet enim tortor at auctor urna. In nibh mauris cursus mattis molestie a iaculis at. Sit amet dictum sit amet justo. Nulla posuere sollicitudin aliquam ultrices sagittis orci. Mus mauris vitae ultricies leo integer malesuada nunc. Natoque penatibus et magnis dis parturient montes nascetur ridiculus mus. Sit amet consectetur adipiscing elit pellentesque habitant. Gravida neque convallis a cras semper auctor neque vitae. Ac odio tempor orci dapibus. Aliquam sem et tortor consequat id porta. Urna nunc id cursus metus aliquam. Blandit turpis cursus in hac habitasse platea dictumst quisque sagittis. Nisi vitae suscipit tellus mauris. Consequat ac felis donec et odio. Lacus viverra vitae congue eu consequat ac. Viverra tellus in hac habitasse platea. Arcu ac tortor dignissim convallis aenean et tortor at. Adipiscing commodo elit at imperdiet dui accumsan sit. Ut placerat orci nulla pellentesque dignissim enim sit amet venenatis. Ipsum nunc aliquet bibendum enim facilisis gravida neque convallis. Faucibus interdum posuere lorem ipsum. Molestie nunc non blandit massa enim nec dui nunc mattis. In consectetur a erat nam. Diam sit amet nisl suscipit adipiscing bibendum est ultricies integer. Ultrices tincidunt arcu non sodales neque. Sed sed risus pretium quam vulputate dignissim suspendisse in. Tempor id eu nisl nunc mi ipsum faucibus. Consequat nisl vel pretium lectus quam. Proin fermentum leo vel orci porta non. Sem fringilla ut morbi tincidunt augue interdum velit euismod. Adipiscing vitae proin sagittis nisl rhoncus mattis rhoncus. Orci sagittis eu volutpat odio facilisis mauris. Sed turpis tincidunt id aliquet risus feugiat in. Amet venenatis urna cursus eget nunc scelerisque viverra mauris in. Ipsum dolor sit amet consectetur adipiscing elit ut. Interdum velit laoreet id donec ultrices tincidunt arcu non sodales.",
    date: "2017",
    source: "Meetup",
    venue: "Pixel Labs",
    address: "32 Robinson Road, 239482 Singapore",
    image_url: "https://louisem.com/wp-content/uploads/2016/09/facebook-event-cover-photo.jpg",
    created_timestamp: "Last updated 5min ago",
    keywords: ["baby","yoga","meditation","hiking"],
    num_rsvp: 33,
    organizer: "Apple", 
    url: "http://www.apple.com/"
}

const exampleEvent2 = {
    id: 2,
    name: "Example Title 2",
    description: "Lorem ipsum sed dolor amet ham ham ham etc. and then this happens at the event which is great",
    date: "2019",
    source: "Eventbrite",
    venue: "Level 3",
    address: "32 Robinson Road, 239482 Singapore",
    image_url: "https://louisem.com/wp-content/uploads/2016/09/facebook-event-cover-photo.jpg",
    eventDetailInformation: "Last updated 8min ago",
    keywords: ["baby","yoga","meditation","hiking"],
    num_rsvp: 99,
    organizer: "Google", 
    url: "http://www.eventbrite.com/"
}

const search = {
    search: "volleyball, yoga",
    blacklist: "medittion"
}

let exampleServerResponse = [exampleEvent1,exampleEvent2];